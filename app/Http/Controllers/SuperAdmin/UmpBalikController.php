<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\UmpanBalik;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UmpBalikController extends Controller
{
    public function index() {
        $data = UmpanBalik::select(['id','nama','status','email','no_telepon','pesan'])->get();
        return Datatables::of($data)->make(true);
    }

    function destroy(Request $request)
    {
        try {

            UmpanBalik::destroy($request->dataHapus);
            // Hapus pengguna

            // Redirect ke halaman yang sesuai setelah delete sukses
            return response()->json(['success' => 'Umpan Balik berhasil dihapus']);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ], 500);
        }
    }
}
