@extends('superadmin.template.main')

@section('title', 'Data Unit - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Unit Kerja</h6>

                            {{-- Pesan success jika Unit Kerja berhasil ditambahakan --}}
                            @if (session('success'))
                                <div id="success-message" class="alert alert-success">
                                    {{ session('success') }}
                                </div>

                                <script>
                                    // Menghilangkan pesan setelah 5 detik
                                    setTimeout(function() {
                                        document.getElementById('success-message').style.display = 'none';
                                    }, 5000); // 5000 milidetik = 5 detik
                                </script>
                            @endif
                            {{-- end pesan sukses --}}

                            <div class="bt-group">
                                <button type="button" id="bt-tambah" class="btn btn-success btn-sm btn-icon-text"
                                    data-bs-toggle="modal" data-bs-target="#modalTambah"><i class="link-icon"
                                        data-feather="plus-square"></i> Tambah Data</button>
                                <button type="button" onclick="hapusData()" class="btn btn-danger btn-sm btn-icon-text"
                                    id="bt-del"><i class="link-icon" data-feather="x-square"></i> Hapus Data</button>
                            </div>
                        </div>

                        {{-- pesan error validasi form tambah --}}
                        @if ($errors->any())
                            <div id="errorAlert" class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <script>
                                setTimeout(function() {
                                    document.getElementById('errorAlert').style.display = 'none';
                                }, 5000); // Menghilangkan pesan error setelah 5 detik (5000 milidetik)
                            </script>
                        @endif
                        {{-- End Pesan error --}}

                        <div class="table-responsive">
                            <table id="tabelUnit" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="" data-id="' + row.id +'"
                                                class="form-check-input check-all"></th>
                                        <th>Nama Unit</th>
                                        <th>Jumlah</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    @foreach ($jumlahUnit as $unit => $jumlah)
                                        <tr>
                                            <td><input type="checkbox" class="form-check-input check"></td>
                                            <td>{{ $unit }}</td>
                                            <td>{{ $jumlah }}</td>
                                            <td><button type="button" id="bt-detail"
                                                    class="btn btn-secondary btn-sm btn-icon-text"><i class="link-icon"
                                                        data-feather="eye" data-bs-toggle="modal"
                                                        data-bs-target="#modalDetail"></i> </button>
                                                <button type="button" id="bt-edit"
                                                    class="btn btn-success btn-sm btn-icon-text"><i class="link-icon"
                                                        data-feather="edit" data-bs-toggle="modal"
                                                        data-bs-target="#modalEdit"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTambahLabel">Tambah Unit Baru
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('unitstore') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="namaUnit" class="form-label">Nama Unit Kerja</label>
                            <input type="text" class="form-control" id="namaUnitKerja" name="unit_kerja_karyawan"
                                placeholder="Masukkan Nama Unit">
                        </div>
                        @error('unit_kerja_karyawan')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        {{-- <div class="mb-3">
                            <label for="jumlahAnggota" class="form-label">Jumlah Anggota</label>
                            <input type="number" class="form-control" id="jumlahAnggota" name="jumlahanggota"
                                placeholder="Masukkan Jumlah Anggota">
                        </div> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="modalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Edit Unit
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEdit" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="namaUnit" class="form-label">Nama Unit</label>
                            <input type="text" class="form-control" id="namaUnitKerjaEdit" name="unit_kerja_karyawan"
                                placeholder="Masukkan Nama Unit">
                        </div>
                        {{-- <div class="mb-3">
                            <label for="jumlahAnggota" class="form-label">Jumlah Anggota</label>
                            <input type="number" class="form-control" id="jumlahAnggota"
                                placeholder="Masukkan Jumlah Anggota">
                        </div> --}}
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id" name="id">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Unit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="detailNamaUnitKerja" class="form-label">Nama Unit:</label>
                            <input type="text" class="form-control" id="detailNamaUnitKerja" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailJumlahAnggota" class="form-label">Jumlah Anggota:</label></label>
                            <input type="text" class="form-control" id="detailJumlahAnggota" readonly>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const checkAllCheckbox = document.querySelector('.check-all');
        const checkboxes = document.querySelectorAll('.check');

        checkAllCheckbox.addEventListener('change', function() {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = checkAllCheckbox.checked;
            });
        });

        $(document).ready(function() {
            $('#modalTambah, #modalEdit').on('hidden.bs.modal', function() {
                $(this).find('input[type=text], input[type=number]').val('');
            });

            @if ($errors->any())
                $('#modalTambah').modal('show');
            @endif
        });

        var tabel;
        // read data unit kerja
        $(document).ready(function() {
            tabel = $('#tabelUnit').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('dataunit') }}",
                columns: [{
                        data: 'id',
                        name: 'unit_kerja.id',
                        render: function(data, type, row, meta) {
                            return '<input type="checkbox" name="dataHapus[]" class="form-check-input check ceklis" value="' +
                                data + '" data-id="' + data + '">';
                        },
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'unit_kerja_karyawan',
                        name: 'unit_kerja_karyawan',
                    },
                    {
                        data: 'jumlah',
                        name: 'jumlah',
                    },
                    {
                        data: null,
                        name: 'aksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return `<button type="button" onclick="modalDetail('${row.unit_kerja_karyawan}','${row.jumlah}')"
                                                class="btn btn-secondary btn-sm btn-icon-text"><i class="link-icon"
                                                    data-feather="eye" data-bs-toggle="modal"
                                                    data-bs-target="#modalDetail"></i> </button>
                                    <button type="button" onclick="modalEdit('${row.id}','${row.unit_kerja_karyawan}')" class="btn btn-success btn-sm btn-icon-text"><i
                                                class="link-icon" data-feather="edit" data-bs-toggle="modal"
                                                    data-bs-target="#modalEdit" onclick="#"></i> </button>`;
                        }
                    }
                ],
                aLengthMenu: [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                iDisplayLength: 10,
                language: {
                    search: "",
                    paginate: {
                        previous: "Sebelumnya",
                        next: "Selanjutnya"
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    search: "Cari:",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    zeroRecords: "Tidak ditemukan data yang sesuai",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                    infoFiltered: "(disaring dari _MAX_ entri keseluruhan)"
                },
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true,
                drawCallback: function(settings) {
                    feather.replace();

                    // $('#formEdit').on('submit', function(e) {
                    //     e.preventDefault();
                    //     let data = $(this).serialize();

                    //     $.ajax({
                    //         url: "{{ route('unit.update') }}",
                    //         type: "POST",
                    //         data: data,
                    //         success: function(response) {
                    //             console.log(response);
                    //             Swal.fire({
                    //                 title: 'Berhasil',
                    //                 text: 'Data berhasil diubah',
                    //                 icon: 'success',
                    //                 confirmButtonText: 'OK'
                    //             });
                    //             $('#modalEdit').modal('hide');
                    //             tabel.ajax.reload();
                    //         },
                    //         error: function(xhr) {
                    //             console.log(xhr.responseJSON.message);
                    //             Swal.fire({
                    //                 title: 'Gagal',
                    //                 text: xhr.responseJSON.message,
                    //                 icon: 'error',
                    //                 confirmButtonText: 'OK'
                    //             });

                    //         }
                    //     });
                    // });

                    // function modalEdit(id, unit_kerja_karyawan) {
                    //     $('#id').val(id);
                    //     $('#namaUnitKerjaEdit').val(unit_kerja_karyawan);
                    //     $('#modalEdit').modal('show');
                    // }

                    // function modalDetail(unit_kerja_karyawan, jumlah) {
                    //     $('#detailNamaUnitKerja').val(unit_kerja_karyawan);
                    //     $('#detailJumlahAnggota').val(jumlah);
                    //     // $('#detailStatus').text(status); 
                    //     $('#modalDetail').modal('show');
                    // }

                    // $(document).on('click', '.detail-btn', function() {
                    //     var unit_kerja_karyawan = $(this).data('unit_kerja_karyawan');
                    //     var jumlah = $(this).data('jumlah');
                    //     // var status = $(this).data('status');

                    //     modalDetail(unit_kerja_karyawan, jumlah);
                    // });
                },
                initComplete: function() {
                    feather.replace(); // Pastikan feather icons di-load setelah DataTables siap
                }
            });

            $('#tabelUnit').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });

            // Check all checkboxes functionality
            $('.check-all').on('change', function() {
                var isChecked = $(this).is(':checked');
                $('.check').prop('checked', isChecked);
            });

            tabel.on('responsive-display.dt', function(e, datatable, row, showHide, update) {
                feather.replace();
            });
        });

        // hapus data pengguna
        function hapusData() {
            var dataHapus = [];
            $('.ceklis:checked').each(function() {
                dataHapus.push($(this).val());
            });

            if (dataHapus.length > 0) {
                Swal.fire({
                    title: 'Anda yakin?',
                    text: 'Data yang dihapus tidak dapat dikembalikan!',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: 'Ya, hapus!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "{{ route('unit.delete') }}",
                            type: "DELETE",
                            data: {
                                dataHapus: dataHapus,
                                _token: "{{ csrf_token() }}",
                                _method: "DELETE"
                            },
                            success: function(response) {
                                console.log(response);
                                Swal.fire({
                                    title: 'Berhasil',
                                    text: 'Data berhasil dihapus',
                                    icon: 'success',
                                    confirmButtonText: 'OK'
                                });
                                tabel.ajax.reload();
                            },
                            error: function(xhr) {
                                console.log(xhr.responseJSON.message);
                                Swal.fire({
                                    title: 'Gagal',
                                    text: xhr.responseJSON.message,
                                    icon: 'error',
                                    confirmButtonText: 'OK'
                                });
                            }
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Peringatan',
                    text: 'Pilih data yang akan dihapus',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                });
            }
        }


        $('#formEdit').on('submit', function(e) {
            e.preventDefault();
            let data = $(this).serialize();

            $.ajax({
                url: "{{ route('unit.update') }}",
                type: "POST",
                data: data,
                success: function(response) {
                    console.log(response);
                    Swal.fire({
                        title: 'Berhasil',
                        text: 'Data berhasil diubah',
                        icon: 'success',
                        confirmButtonText: 'OK'
                    });
                    $('#modalEdit').modal('hide');
                    tabel.ajax.reload();
                },
                error: function(xhr) {
                    console.log(xhr.responseJSON.message);
                    Swal.fire({
                        title: 'Gagal',
                        text: xhr.responseJSON.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    });

                }
            });
        });

        function modalEdit(id, unit_kerja_karyawan) {
            $('#id').val(id);
            $('#namaUnitKerjaEdit').val(unit_kerja_karyawan);
            $('#modalEdit').modal('show');
        }

        function modalDetail(unit_kerja_karyawan, jumlah) {
            $('#detailNamaUnitKerja').val(unit_kerja_karyawan);
            $('#detailJumlahAnggota').val(jumlah);
            // $('#detailStatus').text(status); 
            $('#modalDetail').modal('show');
        }

        $(document).on('click', '.detail-btn', function() {
            var unit_kerja_karyawan = $(this).data('unit_kerja_karyawan');
            var jumlah = $(this).data('jumlah');
            // var status = $(this).data('status');

            modalDetail(unit_kerja_karyawan, jumlah);
        });

        $(window).resize(function() {
            $('#tabelUnit').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush

@push('style')
    <style>
        .btn-secondary {
            margin-right: 5px;
        }

        .link-icon {
            max-width: 20px;
        }

        #bt-tambah {
            margin-right: 10px;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        #tabelUnit thead th:first-child {
            cursor: default;
        }

        #tabelUnit thead th:first-child::after,
        #tabelUnit thead th:first-child::before {
            display: none !important;
            pointer-events: none;
        }

        #tabelUnit td,
        #tabelUnit th {
            text-align: center;
        }

        #tabelUnit td.child {
            text-align: left;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        @media only screen and (max-width: 768px) {
            #tabelUnit td {
                white-space: normal;
                word-wrap: break-word;
            }

            #tabelUnit_filter {
                margin-top: 10px;
            }
        }

        @media only screen and (max-width: 556px) {
            #top-content {
                flex-direction: column;
            }

            .bt-group {
                flex-direction: column;
            }

            #bt-tambah {
                width: 100%;
                margin-top: 10px;
            }

            #bt-del {
                width: 100%;
                margin-top: 10px;
            }
        }
    </style>
@endpush
